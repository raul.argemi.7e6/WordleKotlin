# Wordle

Wordle consiste en adivinar una palabra de 5 letras donde dependiendo de si adivinas o no te pinta las letras de un color u otro.

# Dificultades

La mayor dificultad al hacer esto es conseguir que pinte las letras del color que toque siempre y que separar las funciones funcionen todas las variables y no den error al separarlo.

# Conclusions

Tras muchas pruebas creo que con mis conocimientos cumple con lo que pide y funciona bastante bien.
